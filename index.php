<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>Elementos formulario</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--Especificación del CSS -->
    <link rel="stylesheet" type="text/css" href="css/estilo.css"> <!--NO EDITAR CONTENIDO -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    
</head>
<body>  
    <!-- Barra de navegación -->
    <div class="w3-top">
    <div class="w3-bar w3-theme w3-top w3-left-align w3-large">
        <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1" href="javascript:void(0)" onclick="w3_open()"><i class="fa fa-bars"></i></a>
        <a href="#" class="w3-bar-item w3-button w3-theme-l1">Sistema</a>

    </div>
    </div>

    <!-- Barra menú lateral -->
    <nav class="w3-sidebar w3-bar-block w3-collapse w3-large w3-theme-l5 w3-animate-left" id="mySidebar">
    <a href="javascript:void(0)" onclick="w3_close()" class="w3-right w3-xlarge w3-padding-large w3-hover-black w3-hide-large" title="Close Menu">
        <i class="fa fa-remove"></i>
    </a>
    <h4 class="w3-bar-item"><b>Opciones</b></h4>
    <a class="w3-bar-item w3-button w3-hover-black" href="#">Ingresar</a>
    <a class="w3-bar-item w3-button w3-hover-black" href="#">Registrarse</a>
    </nav>

    <!-- Overlay effect when opening sidebar on small screens -->
    <div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

    <!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
    <div class="w3-main" style="margin-left:250px">

    <div class="w3-row w3-padding-64">
        <div class="w3-twothird w3-container">
        <h1 class="w3-text-teal">Radios y Check</h1><br>
        <form class="FormularioRatio" action="">
            <h3 class="w3-text-teal" id="preguntas1">Preguntas 1???</h1>
            <input type="radio" name="p1" value="r1">Opción A<br>
            <input type="radio" name="p1" value="r2">Opción B<br>
            <input type="radio" name="p1" value="r3">Opción C<br>
            <input type="radio" name="p1" value="r4">Opción D
            <br>
            <h3 class="w3-text-teal" id="preguntas2">Preguntas 2???</h1>
            <input type="radio" name="p2" id="p2" value="A">Opción A<br>
            <input type="radio" name="p2" id="p2" value="B">Opción B<br>
            <input type="radio" name="p2" id="p2" value="C">Opción C<br>
            <input type="radio" name="p2" id="p2" value="D">Opción D
            <br>
            <h3 class="w3-text-teal" id="preguntas2">Preguntas 3???</h1>
            <input type="checkbox" name="p3" id="c1" value="A">Opción A<br>
            <input type="checkbox" name="p3" id="c2" value="B">Opción B<br>
            <input type="checkbox" name="p3" id="c3" value="C">Opción C<br>
            <input type="checkbox" name="p3" id="c4" value="D">Opción D
        </form>
        <input type="button" value="Verificar" id="btnVerificar" onclick="operacion();">
        </div>
        <div class="w3-third w3-container">
        <p class="w3-border w3-padding-large w3-padding-32 w3-center">Bienvenido</p>
        </div>
    </div>

    <!-- Paginación -->
    <div class="w3-center w3-padding-32">
        <div class="w3-bar">
        <!--a class="w3-button w3-black" href="#">1</a-->
        </div>
    </div>
 
    <footer id="myFooter">
        <div class="w3-container w3-theme-l2 w3-padding-32">
            <h4>Sistema</h4>
        </div>
        <div class="w3-container w3-theme-l1">
            <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a></p>
        </div>
    </footer>

    <script
    src="https://code.jquery.com/jquery-3.1.1.min.js"
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/estilos.js"></script> <!--NO EDITAR CONTENIDO -->
    <script type="text/javascript" src="js/index.js"></script>
</body>
</html>
